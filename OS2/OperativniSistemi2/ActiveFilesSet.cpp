#include "ActiveFilesSet.h"

ActiveFilesSet* ActiveFilesSet::singleton = nullptr;
HANDLE ActiveFilesSet::setSemaphore = CreateSemaphore(
	NULL, 1, 32, NULL
);

bool ActiveFilesSetEntry::cmp(const ActiveFilesSetEntry& lv, const ActiveFilesSetEntry& rv)
{
	return strcmp(lv.name, rv.name) < 0;
}

ActiveFilesSetEntry::ActiveFilesSetEntry(char* name, DirDataEntry* fileInfo)
{
	strncpy(this->name, name, 12);
	this->fileInfo = fileInfo;

	if (fileInfo != nullptr)
	{
		fileSemaphore = CreateSemaphore(
			NULL, 1, 32, NULL
		);
	}
}

ActiveFilesSet::ActiveFilesSet()
{
	activeFiles = new std::set<ActiveFilesSetEntry, cmpFunctionPointer>(ActiveFilesSetEntry::cmp);
}

ActiveFilesSet* ActiveFilesSet::getActiveFilesSet()
{
	if (setSemaphore == NULL)
	{
		printf("Semaphore could not be created!\n");
		exit(GetLastError());
	}

	WaitForSingleObject(setSemaphore, INFINITE);
	if (singleton == nullptr)
	{
		singleton = new ActiveFilesSet();
	}
	ReleaseSemaphore(setSemaphore, 1, NULL);

	return singleton;
}

void ActiveFilesSet::deleteActiveFilesSet()
{
	delete singleton;
	singleton = nullptr;
}

ActiveFilesSet::~ActiveFilesSet()
{
	CloseHandle(setSemaphore);
	delete activeFiles;
	activeFiles = nullptr;
}

void ActiveFilesSet::openFile(DirDataEntry *file)
{
	if (file == nullptr)
		return;

	HANDLE sem = nullptr;

	char fullName[13] = { 0 };
	strncat(fullName, file->name, 8);
	strcat(fullName, ".");
	strncat(fullName, file->extension, 3);

	WaitForSingleObject(setSemaphore, INFINITE);
	ActiveFilesSetEntry entry(fullName, nullptr);
	auto iter = activeFiles->find(entry);
	if ( iter != activeFiles->end() )
	{
		sem = iter->fileSemaphore;
		++(*(iter->numOfThreadsWaiting));
	}
	else
	{
		//ako fajl nije otvaran
		ActiveFilesSetEntry newEntry(fullName, file);
		this->activeFiles->insert(newEntry);
		sem = newEntry.fileSemaphore;
	}
	
	ReleaseSemaphore(setSemaphore, 1, NULL);
	WaitForSingleObject(sem, INFINITE);
}

bool ActiveFilesSet::fileActive(char* name)
{
	WaitForSingleObject(setSemaphore, INFINITE);
	bool ret = false;
	ActiveFilesSetEntry entry(name, nullptr);
	if (activeFiles->find(entry) != activeFiles->end())
		ret = true;
	ReleaseSemaphore(setSemaphore, 1, NULL);
	return ret;
}

char ActiveFilesSet::closeFile(char* name)
{
	char ret = 0;
	WaitForSingleObject(setSemaphore, INFINITE);
	ActiveFilesSetEntry entry(name, nullptr);
	auto iter = activeFiles->find(entry);
	if(iter == activeFiles->end())
	{
		ReleaseSemaphore(setSemaphore, 1, NULL);
		return ret;
	}
	
	--(*(iter->numOfThreadsWaiting));
	if (*iter->numOfThreadsWaiting == 0)
	{
		CloseHandle(iter->fileSemaphore);
		delete iter->fileInfo;
		activeFiles->erase(iter);

		if (activeFiles->size() == 0)
		{
			ret = 1;
		}
	}
	else
	{
		ReleaseSemaphore(iter->fileSemaphore, 1, NULL);
	}

	ReleaseSemaphore(setSemaphore, 1, NULL);
	return ret;
}

bool ActiveFilesSet::noFilesActive()
{
	//mozda treba pod mutex
	return activeFiles->size() == 0;
}

char ActiveFilesSet::updateEntry(char* name, DirDataEntry* fileInfo)
{
	WaitForSingleObject(setSemaphore, INFINITE);
	ActiveFilesSetEntry entry(name, nullptr);
	auto iter = activeFiles->find(entry);
	if (iter == activeFiles->end())
	{
		ReleaseSemaphore(setSemaphore, 1, NULL);
		return 0;
	}
	
	iter->fileInfo->fileSize = fileInfo->fileSize;

	ReleaseSemaphore(setSemaphore, 1, NULL);
	return 1;
}

DirDataEntry ActiveFilesSet::getVal(char* name)
{
	WaitForSingleObject(setSemaphore, INFINITE);
	ActiveFilesSetEntry entry(name, nullptr);
	auto iter = activeFiles->find(entry);
	if (iter == activeFiles->end())
	{
		ReleaseSemaphore(setSemaphore, 1, NULL);
		return 0;
	}

	DirDataEntry ret = *iter->fileInfo;

	ReleaseSemaphore(setSemaphore, 1, NULL);
	return ret;
}