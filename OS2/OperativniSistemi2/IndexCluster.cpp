#include "IndexCluster.h"
#include "KernelFS.h"

//0 - error because cluster 0 is taken by bitvector
ClusterNo IndexCluster::getIndex(int pos)
{
	//index out of scope
	if (pos < 0 || pos > LastIndexFile)
		return 0;
	return indexTable[pos];
}

//1 - error
int IndexCluster::setIndex(int pos, ClusterNo entry)
{
	//index out of scope
	if (pos < 0 || pos > LastIndexFile) 
		return 1;
	indexTable[pos] = entry;
	return 0;
}

bool IndexCluster::isIndexEmpty()
{
	for (int i = 0; i < IndexEntries; i++)
	{
		if (indexTable[i] != 0)
			return false;
	}
	return true;
}

//little endian
IndexCluster::IndexCluster(unsigned char* clusterBuff)
{
	for (int i = 0; i < IndexEntries; i++)
	{
		ClusterNo result = 0;
		for (int j = 0; j < sizeof(ClusterNo); j++)
		{
			result |= clusterBuff[i * sizeof(ClusterNo) + j] << j * BYTESIZE;
		}
		indexTable[i] = result;
	}
}

void IndexCluster::IndexToCluster(unsigned char* clusterBuff)
{
	for (int i = 0; i < IndexEntries; i++)
	{
		for (int j = 0; j < sizeof(ClusterNo); j++)
		{
			clusterBuff[i * sizeof(ClusterNo) + j] = indexTable[i] >> j * BYTESIZE;
		}
	}
}