#ifndef INDEXCLUSTER
#define INDEXCLUSTER

#include "part.h"

const unsigned long long IndexEntries = ClusterSize / sizeof(ClusterNo);
const unsigned int LastIndexFile = 511;

class IndexCluster
{
	ClusterNo indexTable[IndexEntries];
public:
	IndexCluster()
	{
		//init Index to all zeros
		for (int i = 0; i < IndexEntries; i++)
			indexTable[i] = 0;
	}
	IndexCluster(unsigned char* clusterBuff);
	void IndexToCluster(unsigned char* clusterBuff);
	ClusterNo getIndex(int pos);
	int setIndex(int pos, ClusterNo entry);
	bool isIndexEmpty();
	~IndexCluster() {}
};
#endif // !INDEXCLUSTER