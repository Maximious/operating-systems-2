#include "DirData.h"
#include "KernelFS.h"

//init entry to all zeros already done 
DirDataEntry::DirDataEntry()
{

}

DirDataEntry::DirDataEntry(DirDataEntry& entry)
{
	strncpy(name, entry.name, MaxFileNameLen);
	strncpy(extension, entry.extension, MaxExtNameLen);
	firstFileCluster = entry.firstFileCluster;
	fileSize = entry.fileSize;
	strncpy(extraInfo, entry.extraInfo, ExtraInfoLen);
}

DirDataEntry::DirDataEntry( char buffer[32] )
{
	int offset = 0;
	for (int i = 0; i < MaxFileNameLen; i++)
	{
		name[i] = buffer[offset++];
	}
	for (int i = 0; i < MaxExtNameLen; i++)
	{
		extension[i] = buffer[offset++];
	}
	offset++; //useless byte
	ClusterNo result = 0;
	for (int i = 0; i < sizeof(ClusterNo); i++)
	{
		result |= (unsigned char)buffer[offset++] << i * BYTESIZE;
	}
	firstFileCluster = result;
	result = 0;
	for (int i = 0; i < sizeof(ClusterNo); i++)
	{
		result |= (unsigned char)buffer[offset++] << i * BYTESIZE;
	}
	fileSize = result;
	for (int i = 0; i < ExtraInfoLen; i++)
	{
		extraInfo[i] = buffer[offset++];
	}
}

void DirDataEntry::toBuff(char* buf)
{
	int offset = 0;
	for (int i = 0; i < MaxFileNameLen; i++)
	{
		buf[offset++] = name[i];
	}
	for (int i = 0; i < MaxExtNameLen; i++)
	{
		buf[offset++] = extension[i];
	}
	buf[offset++] = 0x00; //useless byte
	for (int i = 0; i < sizeof(ClusterNo); i++)
	{
		buf[offset++] = (char) (firstFileCluster >> i * BYTESIZE);
	}
	for (int i = 0; i < sizeof(ClusterNo); i++)
	{
		buf[offset++] = (char) ( fileSize >> i * BYTESIZE );
	}
	for (int i = 0; i < ExtraInfoLen; i++)
	{
		buf[offset++] = extraInfo[i];
	}
}

//error if name[0]==0x00
DirDataEntry DirData::getFile(int pos)
{
	DirDataEntry ent;
	ent.name[0] = 0x00;
	if (pos < 0 || pos >= DirDataEntries) 
		return ent;
	return indexTable[pos];
}

int DirData::setFile(int pos, DirDataEntry entry)
{
	if (pos < 0 || pos >= DirDataEntries)
		return 1;
	indexTable[pos] = entry;
	return 0;
}

//little endian
DirData::DirData(unsigned char* clusterBuff)
{
	for (int i = 0; i < DirDataEntries; i++)
	{
		//offset pointer for sizeof entry bytes
		DirDataEntry result((char* )clusterBuff + i * sizeof(DirDataEntry));
		indexTable[i] = result;
	}
}

void DirData::DirToCluster(unsigned char* clusterBuff)
{
	char buff[32];
	for (int i = 0; i < DirDataEntries; i++)
	{
		indexTable[i].toBuff(buff);
		for (int j = 0; j < sizeof(DirDataEntry); j++)
		{
			clusterBuff[i * sizeof(DirDataEntry) + j] = buff[j];
		}
	}
}

bool DirData::entryTaken(int pos)
{
	if (pos < 0 || pos >= DirDataEntries)
		return true;
	if (indexTable[pos].name[0] != 0x00)
		return true;
	return false;
}

bool DirData::entryValid(int pos)
{
	if (pos < 0 || pos >= DirDataEntries)
		return false;
	DirDataEntry entry = indexTable[pos];
	for (int i = 0; i < MaxFileNameLen; i++)
		if (entry.name[i] != 0x00)
			return true;
	for (int i = 0; i < MaxExtNameLen; i++)
	{
		if (entry.extension[i] != 0x00)
			return true;
	}
	if (entry.firstFileCluster != 0 || entry.fileSize != 0)
		return true;
	for (int i = 0; i < ExtraInfoLen; i++)
	{
		if (entry.extraInfo[i] != 0)
			return true;
	}
	return false;
}

char DirData::freeEntry(int pos)
{
	if (pos < 0 || pos >= DirDataEntries)
		return 1;
	indexTable[pos].name[0] = 0x00;
	return 0;
}

int DirData::getFreeSlot()
{
	for (int i = 0; i < DirDataEntries; i++)
	{
		if (!entryValid(i) || !entryTaken(i))
			return i;
	}
	return -1;
}