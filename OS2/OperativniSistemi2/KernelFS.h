#define _CRT_SECURE_NO_WARNINGS

#ifndef KERNELFILES
#define KERNELFILES

#include "part.h"
#include "FS.h"
#include "DirData.h"
#include "KernelFile.h"
#include "ActiveFilesSet.h"
#include <fstream>
#include <Windows.h>

typedef char BytePos;
extern const char BYTESIZE;

const unsigned long ClusterBitsInCluster = ClusterSize * BYTESIZE;
//probaj da mountedPartition maknes u listu sa fajlovima tj cache
class KernelFS
{
	static HANDLE fileSystemMutex;
	static HANDLE mountingSemaphore;
	static int threadsWaitingToMount;
	static bool unmountInProgress;
	static HANDLE unmountingSemaphore;
	static bool formatInProgress;
	static HANDLE formattingSemaphore;

	friend class KernelFile;
	static ActiveFilesSet* activeFiles;
	static char readCluster(ClusterNo, char*);
	static char writeCluster(ClusterNo, char*);
	static ClusterNo clustersAvailable;
	static ClusterNo rootClusterNumber;
	static char formMask(char requiredBit);
	static char setClusterTaken(ClusterNo cluster);
	static char setClusterFree(ClusterNo cluster);
	// 0 - just find, 1 - delete, 3 - update
	static char findEntry(char* fname, DirDataEntry* entryInfo, char opType);
	static char updateEntry(char* fname, DirDataEntry* entryInfo);
	static char addFile(DirDataEntry entryInfo);
	//generate fileName and extension from abs name
protected:
	KernelFS() {};
	static Partition* mountedPartition;
public:
	static char mount(Partition* partition); 
	static char unmount();
	static char format(); 
	static FileCnt readRootDir();
	// vraca -1 u slucaju neuspeha ili broj fajlova u slucaju uspeha
	static char doesExist(char* fname);
	static File* open(char* fname, char mode);
	static char deleteFile(char* fname);
	static ClusterNo findFreeCluster();
	~KernelFS() {};
};
#endif // !KERNELFILES
