#ifndef KERNELFILE
#define KERNELFILE

#include "File.h"
#include "IndexCluster.h"
#include "KernelFS.h"

const unsigned long BytesInIndexTwoEntry = ClusterSize;
const unsigned long BytesInIndexOneEntry = IndexEntries * BytesInIndexTwoEntry;
const unsigned long MaxNumOfBytesInFile = IndexEntries * BytesInIndexOneEntry;

class KernelFile
{
	DirDataEntry fileInfo;
	char opType;
public:
	KernelFile(DirDataEntry fileInfo, char opType);
	~KernelFile(); //zatvaranje fajla
	char write(BytesCnt, char* buffer);
	BytesCnt read(BytesCnt, char* buffer);
	char seek(BytesCnt);
	BytesCnt filePos();
	char eof();
	BytesCnt getFileSize();
	char truncate();
};

#endif // !KERNELFILE
