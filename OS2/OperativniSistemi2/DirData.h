#ifndef DIRDATA
#define DIRDATA

#include "FS.h"
#include "part.h"

struct DirDataEntry
{
	DirDataEntry();
	DirDataEntry(DirDataEntry& entry);
	//little endian
	DirDataEntry(char buf[32]);
	void toBuff(char* buf);

	char name[8] = { 0 };
	char extension[3] = { 0 };
	char uselessByte = 0;
	ClusterNo firstFileCluster = 0;
	BytesCnt fileSize = 0; //in bytes
	char extraInfo[12] = { 0 };
};
const unsigned long long DirDataEntries = ClusterSize / sizeof(DirDataEntry);
const int MaxFileNameLen = 8;
const int MaxExtNameLen = 3;
const int ExtraInfoLen = 12;

//root directory data
class DirData
{
	DirDataEntry indexTable[DirDataEntries];
public:
	DirData()
	{
		//init table to all zeros
		char* bytePointer = (char*) indexTable;
		for (int i = 0; i < DirDataEntries; i++)
			*(bytePointer++) = 0x00;
	}
	DirData(unsigned char* clusterBuff);
	void DirToCluster(unsigned char* clusterBuff);
	
	bool entryTaken(int pos); 
	bool entryValid(int pos);
	DirDataEntry getFile(int pos);
	int setFile(int pos, DirDataEntry entry);
	int getFreeSlot();
	char freeEntry(int pos);
	~DirData() {}
};
#endif // !DIRDATA