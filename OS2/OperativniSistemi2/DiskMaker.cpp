#define _CRT_SECURE_NO_WARNINGS

#include "DiskMaker.h"
#include <fstream>


void DiskMaker::makeDisk(std::string diskName, ClusterNo diskSize)
{
	std::string confFileName = diskName + ".ini";
	std::string datFileName = diskName + ".dat";
	FILE* confFile = fopen(confFileName.c_str(), "w");
	FILE* datFile = fopen(datFileName.c_str(), "w");
	fprintf(confFile, "%s\n%d",datFileName.c_str(),diskSize);
	fclose(confFile);
	fclose(datFile);
}