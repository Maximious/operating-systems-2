#ifndef DMAKER
#define DMAKER

#include <string>
#include "part.h"

class DiskMaker
{
private:
	DiskMaker();
	~DiskMaker() {};
public:
	void static makeDisk(std::string diskName, ClusterNo diskSize); //diskSize in clusters
};
#endif // !DMAKER
