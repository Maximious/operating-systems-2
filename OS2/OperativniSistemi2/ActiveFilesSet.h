#ifndef ACTIVEFILES
#define ACTIVEFILES

#define  _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <set>
#include "DirData.h"

struct ActiveFilesSetEntry
{
	char name[13] = { 0 };
	DirDataEntry* fileInfo = nullptr;
	HANDLE fileSemaphore;
	ActiveFilesSetEntry(char*, DirDataEntry*);
	static bool cmp(const ActiveFilesSetEntry&, const ActiveFilesSetEntry&);
	int* numOfThreadsWaiting = new int(1);
};

typedef bool(*cmpFunctionPointer)(const ActiveFilesSetEntry&, const ActiveFilesSetEntry&);

class ActiveFilesSet
{
	bool unmountingStarted = false;
	static ActiveFilesSet* singleton;
	static HANDLE setSemaphore;
	std::set<ActiveFilesSetEntry, cmpFunctionPointer> *activeFiles;
	ActiveFilesSet();
public:
	static ActiveFilesSet* getActiveFilesSet();
	static void deleteActiveFilesSet();
	~ActiveFilesSet();
	ActiveFilesSet(ActiveFilesSet const&) = delete;
	void operator=(ActiveFilesSet const&) = delete;
	
	char updateEntry(char*, DirDataEntry*);
	void openFile(DirDataEntry*);
	bool fileActive(char*);
	//returns 1 if all files are closed or 0 if not
	char closeFile(char*);
	bool noFilesActive();
	DirDataEntry getVal(char*);
};

#endif // !ACTIVEFILES
