#include "File.h"
#include "KernelFile.h"

KernelFile::KernelFile(DirDataEntry fileInfo, char opType)
{
	this->fileInfo = fileInfo;
	this->opType = opType;
	BytesCnt* cursor = (BytesCnt*) & (this->fileInfo.extraInfo[0]);
	if (opType == 'a')
	{
		*cursor = fileInfo.fileSize;
	}
	else
	{
		*cursor = 0;
	}
	if (opType == 'w')
	{
		truncate();
	}
}

KernelFile::~KernelFile()
{
	char fullName[13] = { 0 };
	strncat(fullName, fileInfo.name, 8);
	strcat(fullName, ".");
	strncat(fullName, fileInfo.extension, 3);

	//update changes
	KernelFS::activeFiles->updateEntry(fullName, &fileInfo);
	KernelFS::updateEntry(fullName, &fileInfo);

	if (KernelFS::activeFiles->closeFile(fullName))
	{
		if (KernelFS::formatInProgress)
			ReleaseSemaphore(KernelFS::formattingSemaphore, 1, NULL);
		else if (KernelFS::formatInProgress)
			ReleaseSemaphore(KernelFS::unmountingSemaphore, 1, NULL);
	}
} //zatvaranje fajla

char KernelFile::write(BytesCnt numOfBytes, char* buffer)
{
	//if read only
	if (opType == 'r')
		return 0;

	if (fileInfo.fileSize + numOfBytes > MaxNumOfBytesInFile)
		return 0;

	char buf[ClusterSize];
	BytesCnt* cursor = (BytesCnt*)&(fileInfo.extraInfo[0]);

	int indexLevelOneEntry = *cursor / BytesInIndexOneEntry;
	int indexLevelTwoEntry = ( *cursor % BytesInIndexOneEntry ) / BytesInIndexTwoEntry;
	int posInCluster = *cursor % ClusterSize;
	BytesCnt bytesWritten = 0;

	KernelFS::readCluster(fileInfo.firstFileCluster, buf);
	IndexCluster indexOneCluster((unsigned char*)buf);
	IndexCluster* indexTwoCluster = nullptr;
	ClusterNo indexTwoClusterNumber = 0;
	ClusterNo dataIndexClusterNumber = 0;

	while (bytesWritten < numOfBytes)
	{
		if (!indexTwoCluster)
		{
			indexTwoClusterNumber = indexOneCluster.getIndex(indexLevelOneEntry);

			if (indexTwoClusterNumber == 0)
			{
				//create new indexTwoCluster
				ClusterNo freeIndexCluster = KernelFS::findFreeCluster();
				if (!freeIndexCluster)
				{
					indexOneCluster.IndexToCluster((unsigned char*)buf);
					KernelFS::writeCluster(fileInfo.firstFileCluster, buf);
					return 0;
				}
				KernelFS::setClusterTaken(freeIndexCluster);

				indexTwoClusterNumber = freeIndexCluster;
				indexTwoCluster = new IndexCluster;
				indexOneCluster.setIndex(indexLevelOneEntry, freeIndexCluster);
			}
			else
			{
				KernelFS::readCluster(indexTwoClusterNumber, buf);
				indexTwoCluster = new IndexCluster((unsigned char*)buf);
			}
		}

		dataIndexClusterNumber = indexTwoCluster->getIndex(indexLevelTwoEntry);

		if (dataIndexClusterNumber == 0)
		{
			ClusterNo freeDataCluster = KernelFS::findFreeCluster();
			if (!freeDataCluster)
			{
				indexTwoCluster->IndexToCluster((unsigned char*)buf);
				KernelFS::writeCluster(indexTwoClusterNumber, buf);
				indexOneCluster.IndexToCluster((unsigned char*)buf);
				KernelFS::writeCluster(fileInfo.firstFileCluster, buf);
				return 0;
			}
			KernelFS::setClusterTaken(freeDataCluster);

			dataIndexClusterNumber = freeDataCluster;
			indexTwoCluster->setIndex(indexLevelTwoEntry, freeDataCluster);
		}
		else
		{
			KernelFS::readCluster(dataIndexClusterNumber, buf);
		}

		BytesCnt curPos = *cursor % ClusterSize;

		BytesCnt bytesAdded = numOfBytes - bytesWritten;
		if (bytesAdded + curPos > ClusterSize)
			bytesAdded = ClusterSize - curPos;

		for (int i = 0; i < bytesAdded; i++)
		{
			buf[curPos++] = buffer[bytesWritten++];
		}

		KernelFS::writeCluster(dataIndexClusterNumber, buf);
		//change cursor
		*cursor += bytesAdded;
		if (*cursor > fileInfo.fileSize)
			fileInfo.fileSize = *cursor;

		indexLevelTwoEntry++;
		if (indexLevelTwoEntry >= IndexEntries)
		{
			indexLevelTwoEntry = 0;
			indexTwoCluster->IndexToCluster((unsigned char*)buf);
			KernelFS::writeCluster(indexTwoClusterNumber, buf);
			delete indexTwoCluster;
			indexTwoCluster = nullptr;
			indexLevelOneEntry++;
		}

	}

	if (indexTwoCluster)
	{
		indexTwoCluster->IndexToCluster((unsigned char*)buf);
		KernelFS::writeCluster(indexTwoClusterNumber, buf);
		delete indexTwoCluster;
		indexTwoCluster = nullptr;
	}
	indexOneCluster.IndexToCluster((unsigned char*)buf);
	KernelFS::writeCluster(fileInfo.firstFileCluster, buf);

	return 1;
}

//under construction
BytesCnt KernelFile::read(BytesCnt numOfBytes, char* buffer)
{
	char buf[ClusterSize];
	BytesCnt* cursor = (BytesCnt*) & (fileInfo.extraInfo[0]);

	int indexLevelOneEntry = *cursor / BytesInIndexOneEntry;
	int indexLevelTwoEntry = (*cursor % BytesInIndexOneEntry) / BytesInIndexTwoEntry;
	int posInCluster = *cursor % ClusterSize;
	BytesCnt bytesRead = 0;

	KernelFS::readCluster(fileInfo.firstFileCluster, buf);
	IndexCluster indexOneCluster((unsigned char*)buf);
	IndexCluster* indexTwoCluster = nullptr;
	ClusterNo indexTwoClusterNumber = 0;
	ClusterNo dataIndexClusterNumber = 0;

	while (bytesRead < numOfBytes)
	{
		if (!indexTwoCluster)
		{
			indexTwoClusterNumber = indexOneCluster.getIndex(indexLevelOneEntry);

			if (indexTwoClusterNumber == 0)
			{
				break;
			}
			else
			{
				KernelFS::readCluster(indexTwoClusterNumber, buf);
				indexTwoCluster = new IndexCluster((unsigned char*)buf);
			}
		}

		dataIndexClusterNumber = indexTwoCluster->getIndex(indexLevelTwoEntry);

		if (dataIndexClusterNumber == 0)
		{
			break;
		}
		else
		{
			KernelFS::readCluster(dataIndexClusterNumber, buf);
		}

		BytesCnt curPos = *cursor % ClusterSize;

		BytesCnt newBytesRead = numOfBytes - bytesRead;
		if (newBytesRead + curPos > ClusterSize)
			newBytesRead = ClusterSize - curPos;
		if (newBytesRead + *cursor > fileInfo.fileSize)
			newBytesRead = fileInfo.fileSize - *cursor;

		for (int i = 0; i < newBytesRead; i++)
		{
			buffer[bytesRead++] = buf[curPos++];
		}

		//change cursor
		*cursor += newBytesRead;
		if (*cursor >= fileInfo.fileSize)
			break;

		indexLevelTwoEntry++;
		if (indexLevelTwoEntry >= IndexEntries)
		{
			indexLevelTwoEntry = 0;
			delete indexTwoCluster;
			indexTwoCluster = nullptr;
			indexLevelOneEntry++;
		}
	}

	if (indexTwoCluster)
	{
		delete indexTwoCluster;
		indexTwoCluster = nullptr;
	}
	
	if (bytesRead < numOfBytes)
		return bytesRead;
	return 1;
}

char KernelFile::seek(BytesCnt newPos)
{
	if (newPos < 0 || newPos > fileInfo.fileSize)
		return 0;
	BytesCnt* cursor = (BytesCnt*) & (fileInfo.extraInfo[0]); 
	*cursor = newPos;
	return 1;
}

BytesCnt KernelFile::filePos()
{
	BytesCnt* cursor = (BytesCnt*) & (fileInfo.extraInfo[0]);
	return *cursor;
}

char KernelFile::eof()
{
	BytesCnt* cursor = (BytesCnt*) &(fileInfo.extraInfo[0]);
	return *cursor == fileInfo.fileSize;
}

BytesCnt KernelFile::getFileSize()
{
	return fileInfo.fileSize;
}

char KernelFile::truncate()
{
	char buf[ClusterSize];
	BytesCnt* cursor = (BytesCnt*) & (fileInfo.extraInfo[0]);

	int indexLevelOneEntry = *cursor / BytesInIndexOneEntry;
	int indexLevelTwoEntry = (*cursor % BytesInIndexOneEntry) / BytesInIndexTwoEntry;
	int startingLevelOne = indexLevelOneEntry;
	int startingLevelTwo = indexLevelTwoEntry;

	KernelFS::readCluster(fileInfo.firstFileCluster, buf);
	IndexCluster indexOneCluster((unsigned char*)buf);
	IndexCluster* indexTwoCluster = nullptr;
	ClusterNo indexTwoClusterNumber = 0;
	ClusterNo dataIndexClusterNumber = 0;

	while (true)
	{
		if (!indexTwoCluster)
		{
			indexTwoClusterNumber = indexOneCluster.getIndex(indexLevelOneEntry);
			if (indexTwoClusterNumber != 0)
			{
				KernelFS::readCluster(indexTwoClusterNumber, buf);
				indexTwoCluster = new IndexCluster((unsigned char*)buf);
			}
			else
			{
				break;
			}
		}

		dataIndexClusterNumber = indexTwoCluster->getIndex(indexLevelTwoEntry);

		if (dataIndexClusterNumber == 0)
		{
			break;
		}
		else
		{
			if (indexLevelTwoEntry != startingLevelTwo ||
				indexLevelOneEntry != startingLevelOne)
			{
				KernelFS::setClusterFree(dataIndexClusterNumber);
				indexTwoCluster->setIndex(indexLevelTwoEntry, 0);
			}
		}

		indexLevelTwoEntry++;
		if (indexLevelTwoEntry >= IndexEntries)
		{
			indexLevelTwoEntry = 0;
			if (indexLevelOneEntry != startingLevelOne)
			{
				KernelFS::setClusterFree(indexTwoClusterNumber);
				indexOneCluster.setIndex(indexLevelOneEntry, 0);
			}
			else
			{
				indexTwoCluster->IndexToCluster((unsigned char*)buf);
				KernelFS::writeCluster(indexTwoClusterNumber, buf);
				delete indexTwoCluster;
				indexTwoCluster = nullptr;
				indexLevelOneEntry++;
			}
		}
	}

	if (indexTwoCluster)
	{
		if (indexLevelOneEntry != startingLevelOne)
		{
			KernelFS::setClusterFree(indexTwoClusterNumber);
			indexOneCluster.setIndex(indexLevelOneEntry, 0);
		}
		else
		{
			indexTwoCluster->IndexToCluster((unsigned char*)buf);
			KernelFS::writeCluster(indexTwoClusterNumber, buf);
			delete indexTwoCluster;
			indexTwoCluster = nullptr;
		}
	}
	indexOneCluster.IndexToCluster((unsigned char*)buf);
	KernelFS::writeCluster(fileInfo.firstFileCluster, buf);

	fileInfo.fileSize = *cursor;

	return 1;
}