#include "KernelFS.h"
#include "DirData.h"
#include "IndexCluster.h"

Partition* KernelFS::mountedPartition = nullptr;
ClusterNo KernelFS::clustersAvailable = 0;
ClusterNo KernelFS::rootClusterNumber = 0;

HANDLE KernelFS::fileSystemMutex = CreateSemaphore(NULL, 1, 1, NULL);
HANDLE KernelFS::mountingSemaphore = CreateSemaphore(NULL, 0, 32, NULL);
int KernelFS::threadsWaitingToMount = 0;
bool KernelFS::unmountInProgress = false;
bool KernelFS::formatInProgress = false;
HANDLE KernelFS::unmountingSemaphore = CreateSemaphore(NULL, 0, 1, NULL);
HANDLE KernelFS::formattingSemaphore = CreateSemaphore(NULL, 0, 1, NULL);
ActiveFilesSet*  KernelFS::activeFiles = ActiveFilesSet::getActiveFilesSet();

const char BYTESIZE = 8;

char KernelFS::format()
{
	if (!mountedPartition) 
		return 0;
	
	WaitForSingleObject(fileSystemMutex, INFINITE);
	if (!activeFiles->noFilesActive())
	{
		formatInProgress = true;
		ReleaseSemaphore(fileSystemMutex, 1, NULL);
		WaitForSingleObject(formattingSemaphore, INFINITE);
		WaitForSingleObject(fileSystemMutex, INFINITE);
	}

	//calculate size of bitVector
	ClusterNo clusterNo = mountedPartition->getNumOfClusters();
	ClusterNo reqClusters = clusterNo / ClusterBitsInCluster;
	reqClusters += clusterNo % ClusterBitsInCluster ? 1 : 0;

	char buffer[ClusterSize];
	ClusterNo clusterToWriteTo = 0;

	//whole cluster filled
	if (reqClusters > ClusterBitsInCluster)
	{
		ClusterNo fullClusters = reqClusters / ClusterBitsInCluster;
		for (int i = 0; i < ClusterSize; i++)
			buffer[i] = 0x00;
		for (unsigned int i = 0; i < fullClusters; i++)
		{
			mountedPartition->writeCluster(clusterToWriteTo++, buffer);
		}
	}
	int wholeBytesNeeded = (reqClusters % ClusterBitsInCluster) / BYTESIZE;
	int leftClusters = (reqClusters % ClusterBitsInCluster) % BYTESIZE;
	for (int i = 0; i < wholeBytesNeeded; i++)
		buffer[i] = 0X00;
	int result = 0;
	for (int i = 0; i < BYTESIZE-1; i++)
	{
		if (!leftClusters)
		{
			result |= 1;
		}
		else
		{
			leftClusters--;
		}
		result <<= 1;
	}
	result |= 1;
	buffer[wholeBytesNeeded] = result;
	for (int i = wholeBytesNeeded + 1; i < ClusterSize; i++)
		buffer[i] = -1;
	mountedPartition->writeCluster(clusterToWriteTo++, buffer);
	for (int i = 0; i <= wholeBytesNeeded; i++)
		buffer[i] = -1;
	while (clusterToWriteTo < reqClusters)
	{
		mountedPartition->writeCluster(clusterToWriteTo++, buffer);
	}

	IndexCluster* rootIndexOne = new IndexCluster();
	rootIndexOne->IndexToCluster((unsigned char*)buffer);
	mountedPartition->writeCluster(clusterToWriteTo, buffer);
	rootClusterNumber = clusterToWriteTo;
	setClusterTaken(clusterToWriteTo);

	formatInProgress = false;
	//if format and unmount are to be done, do them one after another
	if(unmountInProgress)
		ReleaseSemaphore(unmountingSemaphore, 1, NULL);
	ReleaseSemaphore(fileSystemMutex, 1, NULL);

	return 1;
}

char KernelFS::mount(Partition* partition)
{
	if (!partition)
		return 0;

	WaitForSingleObject(fileSystemMutex, INFINITE);
	if (mountedPartition)
	{
		ReleaseSemaphore(fileSystemMutex, 1, NULL);
		WaitForSingleObject(mountingSemaphore, INFINITE);
		WaitForSingleObject(fileSystemMutex, INFINITE);
	}
	KernelFS::mountedPartition = partition;
	clustersAvailable = partition->getNumOfClusters();

	rootClusterNumber = clustersAvailable / ClusterBitsInCluster;
	rootClusterNumber += clustersAvailable % ClusterBitsInCluster ? 1 : 0;
	ReleaseSemaphore(fileSystemMutex, 1, NULL);

	return 1;
}

char KernelFS::unmount()
{
	WaitForSingleObject(fileSystemMutex, INFINITE);
	if (unmountInProgress)
	{
		ReleaseSemaphore(fileSystemMutex, 1, NULL);
		return 0;
	}
	if(formatInProgress || !activeFiles->noFilesActive())
	{
		unmountInProgress = true;
		ReleaseSemaphore(fileSystemMutex, 1, NULL);
		WaitForSingleObject(unmountingSemaphore, INFINITE);
		WaitForSingleObject(fileSystemMutex, INFINITE);
	}
	KernelFS::mountedPartition = nullptr;
	clustersAvailable = 0;
	rootClusterNumber = 0;
	unmountInProgress = false;
	ReleaseSemaphore(mountingSemaphore, 1, NULL);
	ReleaseSemaphore(fileSystemMutex, 1, NULL);
	return 1;
}

FileCnt KernelFS::readRootDir()
{
	if (!mountedPartition || formatInProgress || unmountInProgress)
		return -1;

	//return number of files in root dir

	FileCnt numOfFiles = 0;
	bool searchEnd = false;
	char buf[ClusterSize];

	mountedPartition->readCluster(rootClusterNumber, buf);
	IndexCluster rootCluster((unsigned char*)buf);

	for (int indexOneClusterEntry = 0; indexOneClusterEntry < IndexEntries;
			indexOneClusterEntry++)
	{
		ClusterNo indexTwoClusterNumber = rootCluster.getIndex(indexOneClusterEntry);
		if (indexTwoClusterNumber == 0)
		{
			searchEnd = true;
			break;
		}
		mountedPartition->readCluster(indexTwoClusterNumber, buf);
		IndexCluster* indexTwoCluster = new IndexCluster((unsigned char*)buf);

		for (int indexTwoClusterEntry = 0; indexTwoClusterEntry < IndexEntries;
				indexTwoClusterEntry++)
		{
			ClusterNo dataClusterNumber = indexTwoCluster->getIndex(indexTwoClusterEntry);
			if (dataClusterNumber == 0)
			{
				searchEnd = true;
				break;
			}
			mountedPartition->readCluster(dataClusterNumber, buf);
			DirData* dataCluster = new DirData((unsigned char*)buf);

			for (int fileTableEntry = 0; fileTableEntry < DirDataEntries;
				fileTableEntry++)
			{				
				if (dataCluster->entryValid(fileTableEntry))
				{
					if (dataCluster->entryTaken(fileTableEntry))
					{
						numOfFiles++;
					}
				}
				else
				{
					searchEnd = true;
					break;
				}
			}

			delete dataCluster;

			if (searchEnd)
			{
				break;
			}
		}

		delete indexTwoCluster;

		if (searchEnd)
		{
			break;
		}

	}
	return numOfFiles;
}

// 0 - just find, 1 - delete, 3 - update
//fname requires '/' in the beginning
char KernelFS::findEntry(char* fname, DirDataEntry* entryInfo, char opType)
{
	if (!mountedPartition)
		return 0;

	//one more space for "\0"
	char fileName[9] = { 0 };
	char fileExt[4] = { 0 };
	int pos = strstr(fname, ".") - fname;
	strncpy(fileName, fname + 1, pos - 1);
	//1 for '/' at the beginning of the file 
	strncpy(fileExt, fname + 1 + pos, MaxExtNameLen);

	bool searchEnd = false;
	char buf[ClusterSize];
	char fileFound = 0;

	mountedPartition->readCluster(rootClusterNumber, buf);
	IndexCluster rootCluster((unsigned char*)buf);

	for (int indexOneClusterEntry = 0; indexOneClusterEntry < IndexEntries;
		indexOneClusterEntry++)
	{
		ClusterNo indexTwoClusterNumber = rootCluster.getIndex(indexOneClusterEntry);
		if (indexTwoClusterNumber == 0)
		{
			searchEnd = true;
			break;
		}
		mountedPartition->readCluster(indexTwoClusterNumber, buf);
		IndexCluster* indexTwoCluster = new IndexCluster((unsigned char*)buf);

		for (int indexTwoClusterEntry = 0; indexTwoClusterEntry < IndexEntries;
			indexTwoClusterEntry++)
		{
			ClusterNo dataClusterNumber = indexTwoCluster->getIndex(indexTwoClusterEntry);
			if (dataClusterNumber == 0)
			{
				searchEnd = true;
				break;
			}
			mountedPartition->readCluster(dataClusterNumber, buf);
			DirData* dataCluster = new DirData((unsigned char*)buf);

			for (int fileTableEntry = 0; fileTableEntry < DirDataEntries;
				fileTableEntry++)
			{
				if (dataCluster->entryValid(fileTableEntry))
				{
					if (!strcmp(dataCluster->getFile(fileTableEntry).name, fileName) &&
						!strcmp(dataCluster->getFile(fileTableEntry).extension, fileExt))
					{
						fileFound = 1;
						searchEnd = true;
						if (entryInfo)
							*entryInfo = dataCluster->getFile(fileTableEntry);
						switch (opType)
						{
						case 1:
							//make it free 
							dataCluster->freeEntry(fileTableEntry);
							break;
						default:
							break;
						}
						break;
					}
				}
				else
				{
					searchEnd = true;
					break;
				}
			}

			dataCluster->DirToCluster((unsigned char*)buf);
			mountedPartition->writeCluster(dataClusterNumber, buf);
			delete dataCluster;

			if (searchEnd)
			{
				break;
			}
		}

		delete indexTwoCluster;

		if (searchEnd)
		{
			break;
		}

	}

	return fileFound;
}

char KernelFS::updateEntry(char* fname, DirDataEntry* entryInfo)
{
	if (!mountedPartition)
		return 0;

	WaitForSingleObject(fileSystemMutex, INFINITE);
	//one more space for "\0"
	char fileName[9] = { 0 };
	char fileExt[4] = { 0 };
	int pos = strstr(fname, ".") - fname;
	char* slash = strstr(fname, "/");
	strncpy(fileName, fname, pos);
	//1 for '/' at the beginning of the file 
	strncpy(fileExt, fname + 1 + pos, MaxExtNameLen);

	bool searchEnd = false;
	char buf[ClusterSize];
	char fileFound = 0;

	mountedPartition->readCluster(rootClusterNumber, buf);
	IndexCluster rootCluster((unsigned char*)buf);

	for (int indexOneClusterEntry = 0; indexOneClusterEntry < IndexEntries;
		indexOneClusterEntry++)
	{
		ClusterNo indexTwoClusterNumber = rootCluster.getIndex(indexOneClusterEntry);
		if (indexTwoClusterNumber == 0)
		{
			searchEnd = true;
			break;
		}
		mountedPartition->readCluster(indexTwoClusterNumber, buf);
		IndexCluster* indexTwoCluster = new IndexCluster((unsigned char*)buf);

		for (int indexTwoClusterEntry = 0; indexTwoClusterEntry < IndexEntries;
			indexTwoClusterEntry++)
		{
			ClusterNo dataClusterNumber = indexTwoCluster->getIndex(indexTwoClusterEntry);
			if (dataClusterNumber == 0)
			{
				searchEnd = true;
				break;
			}
			mountedPartition->readCluster(dataClusterNumber, buf);
			DirData* dataCluster = new DirData((unsigned char*)buf);

			for (int fileTableEntry = 0; fileTableEntry < DirDataEntries;
				fileTableEntry++)
			{
				if (dataCluster->entryValid(fileTableEntry))
				{
					if (!strcmp(dataCluster->getFile(fileTableEntry).name, fileName) &&
						!strcmp(dataCluster->getFile(fileTableEntry).extension, fileExt))
					{
						fileFound = 1;
						searchEnd = true;
						dataCluster->setFile(fileTableEntry, *entryInfo);
					}
				}
				else
				{
					searchEnd = true;
					break;
				}
			}

			dataCluster->DirToCluster((unsigned char*)buf);
			mountedPartition->writeCluster(dataClusterNumber, buf);
			delete dataCluster;

			if (searchEnd)
			{
				break;
			}
		}

		delete indexTwoCluster;

		if (searchEnd)
		{
			break;
		}

	}

	ReleaseSemaphore(fileSystemMutex, 1, NULL);
	return fileFound;
}

char KernelFS::doesExist(char* fname)
{
	return findEntry(fname, 0, false);
}

char KernelFS::addFile(DirDataEntry entryInfo)
{
	if (!mountedPartition)
		return 1;

	bool searchEnd = false;
	char buf[ClusterSize];
	char slotFound = 0;

	mountedPartition->readCluster(rootClusterNumber, buf);
	IndexCluster rootCluster((unsigned char*)buf);

	for (int indexOneClusterEntry = 0; indexOneClusterEntry < IndexEntries;
		indexOneClusterEntry++)
	{
		ClusterNo indexTwoClusterNumber = rootCluster.getIndex(indexOneClusterEntry);
		if (indexTwoClusterNumber == 0)
		{
			//make new indexLevelTwo and DirDataClusters
			ClusterNo freeIndexCluster = findFreeCluster();
			if (!freeIndexCluster)
				return 1;
			setClusterTaken(freeIndexCluster);
			ClusterNo freeDataCluster = findFreeCluster();
			if (!freeDataCluster)
			{
				//free index cluster
				setClusterFree(freeIndexCluster);
				//not enough space 
				return 1;
			}
			setClusterTaken(freeDataCluster);

			IndexCluster levelTwoIndex;
			levelTwoIndex.setIndex(0, freeDataCluster);
			DirData fileList;
			fileList.setFile(0, entryInfo);
			fileList.DirToCluster((unsigned char*)buf);
			mountedPartition->writeCluster(freeDataCluster,buf);
			levelTwoIndex.IndexToCluster((unsigned char*)buf);
			mountedPartition->writeCluster(freeIndexCluster, buf);
			rootCluster.setIndex(indexTwoClusterNumber, freeIndexCluster);
			rootCluster.IndexToCluster((unsigned char*)buf);
			mountedPartition->writeCluster(rootClusterNumber, buf);
			return 0;
		}
		mountedPartition->readCluster(indexTwoClusterNumber, buf);
		IndexCluster* indexTwoCluster = new IndexCluster((unsigned char*)buf);

		for (int indexTwoClusterEntry = 0; indexTwoClusterEntry < IndexEntries;
			indexTwoClusterEntry++)
		{
			ClusterNo dataClusterNumber = indexTwoCluster->getIndex(indexTwoClusterEntry);
			if (dataClusterNumber == 0)
			{
				//make new DirDataCluster
				ClusterNo freeDataCluster = findFreeCluster();
				if (!freeDataCluster)
				{
					searchEnd = true;
					break;
				}
				setClusterTaken(freeDataCluster);
				DirData fileTable;
				fileTable.setFile(0, entryInfo);
				indexTwoCluster->setIndex(indexTwoClusterEntry, freeDataCluster);
				fileTable.DirToCluster((unsigned char*)buf);
				mountedPartition->writeCluster(freeDataCluster, buf);
				indexTwoCluster->IndexToCluster((unsigned char*)buf);
				mountedPartition->writeCluster(indexTwoClusterNumber, buf);
				
				return 0;
			}
			mountedPartition->readCluster(dataClusterNumber, buf);
			DirData* dataCluster = new DirData((unsigned char*)buf);

			for (int fileTableEntry = 0; fileTableEntry < DirDataEntries;
				fileTableEntry++)
			{
				if (!dataCluster->entryValid(fileTableEntry) || 
					!dataCluster->entryTaken(fileTableEntry))
				{
					dataCluster->setFile(fileTableEntry, entryInfo);
					dataCluster->DirToCluster((unsigned char*)buf);
					mountedPartition->writeCluster(dataClusterNumber, buf);
					searchEnd = true;
					break;
				}
			}

			delete dataCluster;
			//maybe write to file before delete
			if (searchEnd)
			{
				break;
			}
		}

		delete indexTwoCluster;

		if (searchEnd)
		{
			return 0;
		}

	}

	//no more space
	return 1;
}

File* KernelFS::open(char* fname, char mode)
{
	//ako je particija trenutno u fazi unmount ili format vrati neuspeh
	WaitForSingleObject(fileSystemMutex, INFINITE);
	if (unmountInProgress || formatInProgress)
	{
		ReleaseSemaphore(fileSystemMutex, 1, NULL);
		return 0;
	}
	DirDataEntry* fileInfo = new DirDataEntry();
	char status = findEntry(fname, fileInfo, false);
	//for new file
	DirDataEntry* newFileInfo = new DirDataEntry();

	//one more space for "\0"
	char fileName[9] = { 0 };
	char fileExt[4] = { 0 };
	int pos = strstr(fname, ".") - fname;
	strncpy(fileName, fname + 1, pos - 1);
	//1 for '/' at the beginning of the file 
	strncpy(fileExt, fname + 1 + pos, MaxExtNameLen);

	if (mode != 'w')
	{
		if (status == 0)
		{
			ReleaseSemaphore(fileSystemMutex, 1, NULL);
			return 0;
		}
	}
	else
	{
		if (fileInfo->name[0] == 0x00)
		{
			//create file
			ClusterNo freeCluster = findFreeCluster();
			if (!freeCluster)
				return 0;
			setClusterTaken(freeCluster);
	
			strncpy(newFileInfo->name, fileName, MaxFileNameLen);
			strncpy(newFileInfo->extension, fileExt, MaxExtNameLen);
			newFileInfo->firstFileCluster = freeCluster;
			newFileInfo->fileSize = 0;
			char buf[ClusterSize] = { 0 };
			mountedPartition->writeCluster(freeCluster, buf);

			if (addFile(*newFileInfo))
			{
				//not enough space to add file
				setClusterFree(freeCluster);
				delete fileInfo;
				ReleaseSemaphore(fileSystemMutex, 1, NULL);
				return 0;
			}
		}
	}
	ReleaseSemaphore(fileSystemMutex, 1, NULL);

	KernelFile* newKernelFile;
	if(status)
	{
		activeFiles->openFile(fileInfo);
		//maybe the data has been changed
		*fileInfo = activeFiles->getVal(fname + 1);//remove '/'

		newKernelFile = new KernelFile(*fileInfo, mode);
		delete newFileInfo;
	}
	else
	{
		activeFiles->openFile(newFileInfo);
		newKernelFile = new KernelFile(*newFileInfo, mode);
	}

	//delete fileInfo;
	File* newFile = new File();
	newFile->myImpl = newKernelFile;
	return newFile;
}

char KernelFS::deleteFile(char* fname)
{
	WaitForSingleObject(fileSystemMutex, INFINITE);
	if (!mountedPartition || unmountInProgress || formatInProgress)
	{
		ReleaseSemaphore(fileSystemMutex, 1, NULL);
		return 0;
	}

	//not existing
	DirDataEntry* fileInfo = new DirDataEntry();
	if (!findEntry(fname, fileInfo, true) || fileInfo->firstFileCluster == 0)
		return 0;

	if (activeFiles->fileActive(fname + 1)); // remove '/'

	char buff[ClusterSize];
	bool searchEnd = false;

	mountedPartition->readCluster(fileInfo->firstFileCluster, buff);
	IndexCluster fileIndexOne((unsigned char*)buff);

	for (int indexOneClusterEntry = 0; indexOneClusterEntry < IndexEntries;
			indexOneClusterEntry++)
	{
		ClusterNo indexTwoClusterNumber = fileIndexOne.getIndex(indexOneClusterEntry);
		if (indexTwoClusterNumber == 0)
		{
			searchEnd = true;
			break;
		}
		mountedPartition->readCluster(indexTwoClusterNumber, buff);
		IndexCluster* indexTwoCluster = new IndexCluster((unsigned char*)buff);

		for (int indexTwoClusterEntry = 0; indexTwoClusterEntry < IndexEntries;
			indexTwoClusterEntry++)
		{
			ClusterNo dataClusterNumber = indexTwoCluster->getIndex(indexTwoClusterEntry);
			if (dataClusterNumber == 0)
			{
				searchEnd = true;
				break;
			}
			
			setClusterFree(dataClusterNumber);
		}

		setClusterFree(indexTwoClusterNumber);
		delete indexTwoCluster;

		if (searchEnd)
		{
			break;
		}
	}

	setClusterFree(fileInfo->firstFileCluster);
	ReleaseSemaphore(fileSystemMutex, 1, NULL);
	return 1;
}

char KernelFS::formMask(char requiredBit)
{
	if (requiredBit >= BYTESIZE)
		return 0;
	int result = 0x80;
	for (int i = 0; i < requiredBit; i++)
	{
		result >>= 1;
	}
	return result;
}

char KernelFS::setClusterTaken(ClusterNo cluster)
{
	if (!mountedPartition)
		return 1;

	int requiredCluster = cluster / ClusterBitsInCluster;
	char buff[ClusterSize];
	//add cache and concurrency
	mountedPartition->readCluster(requiredCluster, buff);
	int requiredByte = (cluster % ClusterBitsInCluster) / 8;
	int requiredBit = (cluster % ClusterBitsInCluster) % 8;
	buff[requiredByte] &= ~formMask(requiredBit);
	mountedPartition->writeCluster(requiredCluster, buff);
	return 0;
}

char KernelFS::setClusterFree(ClusterNo cluster)
{
	if (!mountedPartition)
		return 1;

	int requiredCluster = cluster / ClusterBitsInCluster;
	char buff[ClusterSize];
	//add cache and concurrency
	mountedPartition->readCluster(requiredCluster, buff);
	int requiredByte = (cluster % ClusterBitsInCluster) / 8;
	int requiredBit = (cluster % ClusterBitsInCluster) % 8;
	buff[requiredByte] |= formMask(requiredBit);
	mountedPartition->writeCluster(requiredCluster, buff);
	return 0;
}

ClusterNo KernelFS::findFreeCluster()
{
	char buff[ClusterSize];
	int bitVectorClusters = clustersAvailable / ClusterBitsInCluster;
	bitVectorClusters += clustersAvailable % ClusterBitsInCluster ? 1 : 0;

	int i,j,k;
	//finding byte
	for (i = 0; i < bitVectorClusters; i++)
	{
		bool clusterFound = false;
		mountedPartition->readCluster(i, buff);
		for (j = 0; j < ClusterSize; j++)
		{
			if (buff[j] != 0)
			{
				clusterFound = true;
				break;
			}
		}
		if (clusterFound)
		{
			break;
		}
	}
	//finding bit
	unsigned char bitMask = 0x80;
	for (k = 0; k < BYTESIZE; k++)
	{
		//free cluster means 1 in bitVector
		if ((buff[j] & bitMask))
		{
			break;
		}
		bitMask >>= 1;
	}
	ClusterNo freeCluster = i * ClusterBitsInCluster + j * BYTESIZE + k;
	return freeCluster;
}

char KernelFS::readCluster(ClusterNo clusterNum, char* buf)
{
	if (!mountedPartition)
		return 1;
	mountedPartition->readCluster(clusterNum, buf);
	return 0;
}

char KernelFS::writeCluster(ClusterNo clusterNum, char* buf)
{
	if (!mountedPartition)
		return 1;
	mountedPartition->writeCluster(clusterNum, buf);
	return 0;
}